
#!/bin/bash
#Ark survival evolved server management script
lockdir=/Games/Ark

function startserver {
	echo "Backing up first!"
	cd /Games/Ark/ShooterGame/Binaries/Linux/
	date=`date +%Y-%m-%d_%H-%M-%S`
	savedir=/Games/Ark/ShooterGame/Saved/
	backupraw=/Games/Ark/ShooterGame/SavedBackup/
	backupdir=$backupraw/$date/
	mkdir -p $backupdir
	cp $savedir $backupdir -r
	echo -e "Backed up save directory at \n" $savedir "\nto\n" $backupdir
	file=`ls /Games/Ark/ShooterGame/SavedBackup/ | sort -n | head -1`
	echo -e "Deleting backup file for "$file " as it is the oldest file"
	rm -r $backupraw/$file
	echo -e "Deleted "$file
	nice --8 ./ShooterGameServer TheIsland?listen?AllowDeprecatedStructures=true?ModId=506506101,533828549 -nosteamclient -server -log -servergamelog </dev/null &>/dev/null &
	PID=$!
	echo $PID > $lockdir/Lock.PID
	echo "PID of game server should be $PID"
	echo "Server should be ready at about `date +"%r" --date='5 minutes'`"
}

function stopserver {
	echo "sending SIGINT to server, server should take this as a signal to save and close"
	if [ -e $lockdir/Lock.PID ]
	then
		kill -INT `cat $lockdir/Lock.PID`
		rm $lockdir/Lock.PID
	else
		echo "Failed to close server, no Lock.PID file"
		echo "Is the server running?"
	fi
}

function update {
	echo "Updating server"
	if [ -e $lockdir/Lock.PID ]
	then
		echo "Error, lock file exists, the server may be running"
		echo "please stop the server or add the -f argument"
		echo "to the end of $0"
	else
		/Games/steamcmd/steamcmd.sh +login anonymous +force_install_dir /Games/Ark/ +app_update 376030 validate +quit
	fi
	echo "Update complete"
}

function forcerestart {
	if [ -e $lockdir/Lock.PID ]
	then
		echo "lock file exists, forcing server to stop"
		forcestopserver
		wait `cat $lockdir/Lock.PID`
		echo "forcing server to start"
		forcestartserver
	else
		forcestartserver
	fi
}

function restartserver {
	if [ -e $lockdir/Lock.PID ]
	then
		echo "lock file exists, waiting for server to close"
		stopserver
		wait `cat $lockdir/Lock.PID`
		startserver
	else
		startserver
	fi
}

function status {
	if [ -e $lockdir/Lock.PID ]
	then
		#TODO: PRINT STATUS OF RUNNING SERVER
		echo "not implemented"
	else
		echo "No lock file, server may not be running"
		echo "or the lock file has been deleted"
	fi
	echo "status"
}
function forcestartserver {
	rm $lockdir/Lock.PID
	startserver
}

function instructions {
	echo ""
	echo "start command launches ark survival evolved server and"
	echo "    prints the PID of the running server"
	echo "stop command will send SIGINT to the server, requesting that" 
	echo "    it saves and shuts down"
	echo "restart command will close any running instance of the server"
	echo "    and then relaunch the server"
	echo "status command pastes current server stats if running, this"
	echo "    command will state if server is not running"
	echo "update command launches steamcmd to update the ark server"
	echo "    updater will take focus of the console"
	echo "    only run update if new update is present and not installed"
	echo ""
	echo "-f in combination with start, stop or update forces the server to enter that"
	echo "    state (force stop the server for stop or update argument, "
	echo "    force start for start argument)"
	echo "    the -f option will also delete the lock file if any is present"
	echo "    before running the command"
	echo "    this option should only be used if the server has crashed or if the"
	echo "    server has closed via unconventional means (ie power outage)"
	echo "Example:"
	echo "    $0 stop -f"
	echo "    this command will force the server to stop, possibly without saving"
	exit
}

function help {
	echo ""
	echo "use $0 start|stop|restart|status|update [-f]"
	instructions
}

function invalidinput {
	echo ""
	echo "ERROR! no changes have been made"
	echo "Invalid arguments, use $0 start|stop|restart|status|update [-f]"

	instructions
}

if [ $# -lt 1 ]
then
	invalidinput
else
	case "$1" in
		start)
			if [ $# -eq 2 ]
			then
				if [ $2 == "-f" ]
				then
					echo "WARNING: Server has been forcefully started"
					forcestartserver
				fi
			else
				echo "Server startup process has started"
				if [ -e $lockdir/Lock.PID ]
				then
					echo "Lock file exists, is the server running?"
				else
					#backupserver
					startserver
				fi
			fi
		;;
		stop)
			if [ $# -eq 2 ]
			then
				if [ $2 -eq "-f" ]
				then
					forcestopserver
				fi
			else
				stopserver
			fi
		;;
		status)
			status
		;;
		restart)
			restartserver
		;;
		update)
			if [ $# -eq 2 ]
			then
				if [ $2 -eq "-f" ]
				then
					forceupdate
				fi
			else
				update
			fi
		;;
	esac
fi